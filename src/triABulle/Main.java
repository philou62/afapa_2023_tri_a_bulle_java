package triABulle;


public class Main {

	public static void main(String[] args) {
		int[] tab = { 80, 45, 99, 1, 3, 26, 9, 7, 13, 5 };
		int temp;
		boolean flag = false;
		int size = tab.length;

		while (!flag) {
			flag = true;
			for (int i = 0; i < size - 1; i++) {
				if (tab[i] > tab[i + 1]) {
					temp = tab[i];
					tab[i] = tab[i + 1];
					tab[i + 1] = temp;
					flag = false;
				}
			}

		}
		for (int i = 0; i < size; i++) {
			System.out.println(tab[i]);
		}

	}

}
